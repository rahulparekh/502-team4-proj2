grammar TJ;

start
    : statement+;

statement
    : functionstmt
    | ifelse
    | whileloop
    | assignment
    | print
    | expr
    | stackOp
    ;

statement2
    : functionstmt
    | ifelse
    | whileloop
    | assignment
    | print
    | expr
    | stackOp
    ;

functionstmt
    : 'function' ID '(' (term)(','term)* ')' '{' statement+ '}'
    ;

ifelse
    : IF '(' relationalExpr ')' '{' statement+ '}' (ELSE '{' statement2+ '}' )?
    ;

whileloop
    : 'while' '(' relationalExpr ')' '{' statement+ '}'
    ;

assignment
    : VAR? ID '=' expr SEMICOLON #assig
    | VAR? ID SEMICOLON #noassign
    ;

stackOp
    : ID '.' PUSH '(' term ')' SEMICOLON #pushStack
    | (VAR? ID ASSIGN )? ID '.' POP '()' SEMICOLON #popStack
    | VAR? ID ASSIGN STACK SEMICOLON #assignStack
    ;


relationalExpr
    : expr '>' expr #gt
    | expr '<' expr #lt
    | expr '<=' expr #lte
    | expr '>=' expr #gte
    | expr '==' expr #eq
    | expr '!=' expr #neq
    ;

expr
    : expr MULD expr  #muldiv
    | expr PLUSMIN expr  #plusminus
    | expr '(' term ')' SEMICOLON #funcall
    | term #termexpr
    ;


print
    : 'print' term+  SEMICOLON
    ;

term
    : ID #id
    | INT #int
    | BOOL #bool
    | STRING #string
    ;

MULD
    : '*'
    | '/'
    ;

PLUSMIN
    : '+'
    | '-'
    ;

ASSIGN: '=';

SEMICOLON: ';' ;

VAR: 'var';

STACK: 'STACK';
PUSH: 'push';
POP: 'pop';

IF: 'if';

ELSE: 'else';

INT: [0-9]+ ;

BOOL: 'TRUE'
    | 'FALSE'
    ;

ID: [A-Za-z_][A-Za-z0-9_]*;

STRING: '"' [A-Za-z_ ][A-Za-z0-9_ ]* '"';

WS : [ \t\r\n]+ -> skip ;
