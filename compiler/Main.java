
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.antlr.v4.runtime.ANTLRFileStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

public class Main {
    public static void main(String[] args) throws Exception {
    	
    	if(args.length > 0) {
            
            TJLexer lexer = new TJLexer(new ANTLRFileStream(args[0]));
            TJParser parser = new TJParser(new CommonTokenStream(lexer));
            ParseTree tree = parser.start();
            TJMyVisitor visitor = new TJMyVisitor();
            T result = visitor.visit(tree);
            StringBuilder st = new StringBuilder();
            st.append(args[0]);
            st.append(".int");
            
    		FileWriter fw;
    		try {
    			
    			fw = new FileWriter(new File(st.toString()));
    			fw.write(result.toString());
    			fw.close();
    			
    		} catch (IOException ex) {
    			ex.printStackTrace();
    		}
            
        }else{
        	
        	System.out.println("Error! No file found");
        	
        }
    }
}