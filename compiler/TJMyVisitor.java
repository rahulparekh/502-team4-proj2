// Generated from TJ.g4 by ANTLR 4.5
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.AbstractParseTreeVisitor;
import org.stringtemplate.v4.ST;
import org.stringtemplate.v4.STGroup;
import org.stringtemplate.v4.STGroupDir;

public class TJMyVisitor extends TJBaseVisitor<T> {
	
	
	private int cCount = 0;
	
	@Override public T visitStart(TJParser.StartContext ctx) {
		StringBuilder st = new StringBuilder();
		
		for(int i=0; i < ctx.statement().size(); i++ ){
						
			st.append(visit(ctx.statement(i)));
		}
		
		return new T(st.toString()); 
	}
	
	
	@Override public T visitStatement(TJParser.StatementContext ctx) {
		
		return visitChildren(ctx); 
	}
	
	
	@Override public T visitStatement2(TJParser.Statement2Context ctx) { 
		return visitChildren(ctx); 
	}
	
	
	@Override public T visitFunctionstmt(TJParser.FunctionstmtContext ctx) {
		
		StringBuilder st = new StringBuilder();
		st.append("\nBEGIN_FUNCT");
		st.append("\nFUNCTION ");
		
		st.append(ctx.ID());
		st.append(":");
		
		for(int i=0; i < ctx.term().size(); i++ ){
			st.append(visit(ctx.term(i)).asString().substring(5).trim());
			st.append(",");
		}
		
		st.setLength(st.length() - 1); // remove last comma
		
		for(int i=0; i < ctx.statement().size(); i++ ){
			st.append(visit(ctx.statement(i)));
		}		
		
		st.append("\nENDFUNCTION");
		
		return new T(st.toString());
	}
	
	
	@Override public T visitIfelse(TJParser.IfelseContext ctx) {
		
		StringBuilder st = new StringBuilder();
		
		int conditionCount = cCount;
		cCount++;
		int trueCount = cCount;
		
		st.append("\nCONDITION ");
		st.append("C");
		st.append(conditionCount);
		
		st.append(visit(ctx.relationalExpr()));
		st.append("\nENDCONDITION");
		
		st.append("\nIFTRUE ");
		st.append("C");
		st.append(trueCount);
		
		for(int i=0; i < ctx.statement().size(); i++ ){
			st.append(visit(ctx.statement(i)));
		}		
		st.append("\nENDC");
		st.append(trueCount);
		
		if(ctx.ELSE() != null){
			
			cCount++;

			st.append("\nIFFALSE ");
			st.append("C");
			st.append(cCount);
			
			for(int i=0; i < ctx.statement2().size(); i++ ){
				st.append(visit(ctx.statement2(i)));
			}		
			st.append("\nENDC");
			st.append(cCount);
			
		}
		
		cCount++;
		
		return new T(st.toString());
	
	}
	
	
	
	
	@Override public T visitWhileloop(TJParser.WhileloopContext ctx) {
		
		StringBuilder st = new StringBuilder();
		
		st.append("\nCONDITION ");
		st.append("C");
		st.append(cCount);
		st.append(visit(ctx.relationalExpr()));
		st.append("\nENDCONDITION");
		int conditionCount = cCount;
		cCount++;
		st.append("\nIFTRUE ");
		st.append("C");
		st.append(cCount);
		
		for(int i=0; i < ctx.statement().size(); i++ ){
			st.append(visit(ctx.statement(i)));
		}		
		st.append("\nENDC");
		st.append(cCount);
		st.append("\nGOTO,");
		st.append("C");
		st.append(cCount);
		st.append(",C");
		st.append(conditionCount);

		cCount++;
		
		return new T(st.toString());		
		 
	}
	
	
	@Override public T visitAssig(TJParser.AssigContext ctx) {
		
		String id = ctx.ID().getText();
		StringBuilder st = new StringBuilder();		
		st.append(visit(ctx.expr()));
		st.append("\nPUSH ");
		st.append(id);
		st.append("\nASSIGN");
		
		return new T(st.toString());
	}
	
	
	@Override public T visitNoassign(TJParser.NoassignContext ctx) {
		
		String id = ctx.ID().getText();
		
		StringBuilder st = new StringBuilder();
		
		st.append("\nPUSH ");
		st.append(id);
		
		return new T(st.toString());
	}
	
	
	@Override public T visitPushStack(TJParser.PushStackContext ctx) {
		
		StringBuilder st = new StringBuilder();
		
		st.append("\nPUSH ");
		st.append(ctx.term().getText());
		String id = ctx.ID().getText();
		st.append("\nPUSH ");
		st.append(id);
		st.append("\nASSIGNSTACK");
		
		return new T(st.toString());
	}
	
	
	@Override public T visitPopStack(TJParser.PopStackContext ctx) {
		
		
		StringBuilder st = new StringBuilder();
		st.append("\nPOP ");
		
		if(ctx.ID().size() > 1){
			String id = ctx.ID(1).getText();
			st.append(id);
			st.append("\nPUSH ");
			id = ctx.ID(0).getText();
			st.append(id);
			st.append("\nASSIGN");			
		}else{
			String id = ctx.ID(0).getText();
			st.append(id);	
		}
		
		return new T(st.toString());
	}
	
	
	@Override public T visitAssignStack(TJParser.AssignStackContext ctx) {
		
		StringBuilder st = new StringBuilder();
		
		st.append("\nPUSH ");
		st.append("RESSTACK");
		st.append("\nPUSH ");
		String id = ctx.ID().getText();
		st.append(id);
		st.append("\nASSIGN");
		
		return new T(st.toString());
	}
	
	
	@Override public T visitGt(TJParser.GtContext ctx) {
		
		StringBuilder st = new StringBuilder();		
		// st.append("\nPUSH ");
		st.append(visit(ctx.expr(1)));
		// st.append("\nPUSH ");
		st.append(visit(ctx.expr(0)));
		st.append("\n");
		st.append(">");
		
		return new T(st.toString()); 
	}
	
	
	@Override public T visitLt(TJParser.LtContext ctx) {
		
		StringBuilder st = new StringBuilder();		
		// st.append("\nPUSH ");
		st.append(visit(ctx.expr(1)));
		// st.append("\nPUSH ");
		st.append(visit(ctx.expr(0)));
		st.append("\n");
		st.append("<");
		
		return new T(st.toString());
		
	}
	
	
	@Override public T visitLte(TJParser.LteContext ctx) {
		StringBuilder st = new StringBuilder();		
		// st.append("\nPUSH ");
		st.append(visit(ctx.expr(1)));
		// st.append("\nPUSH ");
		st.append(visit(ctx.expr(0)));
		st.append("\n");
		st.append("<=");
		
		return new T(st.toString());
	}
	
	
	@Override public T visitGte(TJParser.GteContext ctx) {
		StringBuilder st = new StringBuilder();		
		// st.append("\nPUSH ");
		st.append(visit(ctx.expr(1)));
		// st.append("\nPUSH ");
		st.append(visit(ctx.expr(0)));
		st.append("\n");
		st.append(">=");
		
		return new T(st.toString());
	}
	
	
	@Override public T visitEq(TJParser.EqContext ctx) {
		StringBuilder st = new StringBuilder();		
		// st.append("\nPUSH ");
		st.append(visit(ctx.expr(1)));
		// st.append("\nPUSH ");
		st.append(visit(ctx.expr(0)));
		st.append("\n");
		st.append("==");
		
		return new T(st.toString());
	}
	
	
	@Override public T visitNeq(TJParser.NeqContext ctx) {
		
		StringBuilder st = new StringBuilder();		
		// st.append("\nPUSH ");
		st.append(visit(ctx.expr(1)));
		// st.append("\nPUSH ");
		st.append(visit(ctx.expr(0)));
		st.append("\n");
		st.append("!=");
		
		return new T(st.toString());
		
	}
	
	
	@Override public T visitPlusminus(TJParser.PlusminusContext ctx) {
		StringBuilder st = new StringBuilder();		
		// st.append("\nPUSH ");
		st.append(visit(ctx.expr(1)));
		// st.append("\nPUSH ");
		st.append(visit(ctx.expr(0)));
		st.append("\n");		
		st.append(ctx.PLUSMIN());
		
		return new T(st.toString());
	}
	
	
	@Override public T visitFuncall(TJParser.FuncallContext ctx) {
		
		StringBuilder st = new StringBuilder();		
		st.append("\nCALL_FUNCT ");
		st.append(visit(ctx.expr()).asString().substring(5).trim());
		st.append(":");
		st.append(visit(ctx.term()).asString().substring(5).trim());		

		return new T(st.toString());
	}
	
	
	@Override public T visitTermexpr(TJParser.TermexprContext ctx) { 
		
		StringBuilder st = new StringBuilder();	
						
		st.append(visit(ctx.term()));		
		
		return new T(st.toString());
		
	}
	
	
	@Override public T visitMuldiv(TJParser.MuldivContext ctx) {
		StringBuilder st = new StringBuilder();		
		// st.append("\nPUSH ");
		st.append(visit(ctx.expr(1)));
		// st.append("\nPUSH ");
		st.append(visit(ctx.expr(0)));
		st.append("\n");		
		st.append(ctx.MULD());
		
		return new T(st.toString()); 
	}
	
	
	@Override public T visitPrint(TJParser.PrintContext ctx) {
		

		StringBuilder st = new StringBuilder();	
		
		for(int i=0; i < ctx.term().size(); i++ ){
			
			// st.append("\nPUSH ");
			st.append(visit(ctx.term(i)));
		}
					
		st.append("\nPRINT");		
		
		return new T(st.toString());
		
	}
	
	
//	@Override public T visitReturnstmt(TJParser.ReturnstmtContext ctx) {
//		
//		StringBuilder st = new StringBuilder();	
//		
//		for(int i=0; i < ctx.term().size(); i++ ){
//			
//			// st.append("\nPUSH ");
//			st.append(visit(ctx.term(i)));
//		}
//					
//		st.append("\nRETURN");		
//		
//		return new T(st.toString());
//		
//	}
	
	
	@Override public T visitId(TJParser.IdContext ctx) { 

		String id = ctx.ID().getText();	
		StringBuilder st = new StringBuilder();				
		st.append("\nPUSH ");
		st.append(id);
		return new T(st.toString());
		
	}
	
	
	@Override public T visitInt(TJParser.IntContext ctx) {
		
		String id = ctx.INT().getText();
		StringBuilder st = new StringBuilder();				
		 st.append("\nPUSH ");
		st.append(id);
		return new T(st.toString());
		
	}
	
	
	@Override public T visitBool(TJParser.BoolContext ctx) { 
		String id = ctx.BOOL().getText();	
		StringBuilder st = new StringBuilder();				
		 st.append("\nPUSH ");
		st.append(id);
		return new T(st.toString()); 
	}
	
	
	@Override public T visitString(TJParser.StringContext ctx) { 
		String id = ctx.STRING().getText();
		StringBuilder st = new StringBuilder();				
		 st.append("\nPUSH ");
		st.append(id);
		return new T(st.toString());  
	}	
}