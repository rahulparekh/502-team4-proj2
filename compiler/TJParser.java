// Generated from TJ.g4 by ANTLR 4.5
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class TJParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.5", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, MULD=17, 
		PLUSMIN=18, ASSIGN=19, SEMICOLON=20, VAR=21, STACK=22, PUSH=23, POP=24, 
		IF=25, ELSE=26, INT=27, BOOL=28, ID=29, STRING=30, WS=31;
	public static final int
		RULE_start = 0, RULE_statement = 1, RULE_statement2 = 2, RULE_functionstmt = 3, 
		RULE_ifelse = 4, RULE_whileloop = 5, RULE_assignment = 6, RULE_stackOp = 7, 
		RULE_relationalExpr = 8, RULE_expr = 9, RULE_print = 10, RULE_term = 11;
	public static final String[] ruleNames = {
		"start", "statement", "statement2", "functionstmt", "ifelse", "whileloop", 
		"assignment", "stackOp", "relationalExpr", "expr", "print", "term"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'function'", "'('", "','", "')'", "'{'", "'}'", "'while'", "'.'", 
		"'()'", "'>'", "'<'", "'<='", "'>='", "'=='", "'!='", "'print'", null, 
		null, "'='", "';'", "'var'", "'STACK'", "'push'", "'pop'", "'if'", "'else'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, "MULD", "PLUSMIN", "ASSIGN", "SEMICOLON", 
		"VAR", "STACK", "PUSH", "POP", "IF", "ELSE", "INT", "BOOL", "ID", "STRING", 
		"WS"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "TJ.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public TJParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class StartContext extends ParserRuleContext {
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public StartContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_start; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TJVisitor ) return ((TJVisitor<? extends T>)visitor).visitStart(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StartContext start() throws RecognitionException {
		StartContext _localctx = new StartContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_start);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(25); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(24);
				statement();
				}
				}
				setState(27); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << T__6) | (1L << T__15) | (1L << VAR) | (1L << IF) | (1L << INT) | (1L << BOOL) | (1L << ID) | (1L << STRING))) != 0) );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StatementContext extends ParserRuleContext {
		public FunctionstmtContext functionstmt() {
			return getRuleContext(FunctionstmtContext.class,0);
		}
		public IfelseContext ifelse() {
			return getRuleContext(IfelseContext.class,0);
		}
		public WhileloopContext whileloop() {
			return getRuleContext(WhileloopContext.class,0);
		}
		public AssignmentContext assignment() {
			return getRuleContext(AssignmentContext.class,0);
		}
		public PrintContext print() {
			return getRuleContext(PrintContext.class,0);
		}
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public StackOpContext stackOp() {
			return getRuleContext(StackOpContext.class,0);
		}
		public StatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statement; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TJVisitor ) return ((TJVisitor<? extends T>)visitor).visitStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StatementContext statement() throws RecognitionException {
		StatementContext _localctx = new StatementContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_statement);
		try {
			setState(36);
			switch ( getInterpreter().adaptivePredict(_input,1,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(29);
				functionstmt();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(30);
				ifelse();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(31);
				whileloop();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(32);
				assignment();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(33);
				print();
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(34);
				expr(0);
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(35);
				stackOp();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Statement2Context extends ParserRuleContext {
		public FunctionstmtContext functionstmt() {
			return getRuleContext(FunctionstmtContext.class,0);
		}
		public IfelseContext ifelse() {
			return getRuleContext(IfelseContext.class,0);
		}
		public WhileloopContext whileloop() {
			return getRuleContext(WhileloopContext.class,0);
		}
		public AssignmentContext assignment() {
			return getRuleContext(AssignmentContext.class,0);
		}
		public PrintContext print() {
			return getRuleContext(PrintContext.class,0);
		}
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public StackOpContext stackOp() {
			return getRuleContext(StackOpContext.class,0);
		}
		public Statement2Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statement2; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TJVisitor ) return ((TJVisitor<? extends T>)visitor).visitStatement2(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Statement2Context statement2() throws RecognitionException {
		Statement2Context _localctx = new Statement2Context(_ctx, getState());
		enterRule(_localctx, 4, RULE_statement2);
		try {
			setState(45);
			switch ( getInterpreter().adaptivePredict(_input,2,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(38);
				functionstmt();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(39);
				ifelse();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(40);
				whileloop();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(41);
				assignment();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(42);
				print();
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(43);
				expr(0);
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(44);
				stackOp();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionstmtContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(TJParser.ID, 0); }
		public List<TermContext> term() {
			return getRuleContexts(TermContext.class);
		}
		public TermContext term(int i) {
			return getRuleContext(TermContext.class,i);
		}
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public FunctionstmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functionstmt; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TJVisitor ) return ((TJVisitor<? extends T>)visitor).visitFunctionstmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FunctionstmtContext functionstmt() throws RecognitionException {
		FunctionstmtContext _localctx = new FunctionstmtContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_functionstmt);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(47);
			match(T__0);
			setState(48);
			match(ID);
			setState(49);
			match(T__1);
			{
			setState(50);
			term();
			}
			setState(55);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__2) {
				{
				{
				setState(51);
				match(T__2);
				setState(52);
				term();
				}
				}
				setState(57);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(58);
			match(T__3);
			setState(59);
			match(T__4);
			setState(61); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(60);
				statement();
				}
				}
				setState(63); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << T__6) | (1L << T__15) | (1L << VAR) | (1L << IF) | (1L << INT) | (1L << BOOL) | (1L << ID) | (1L << STRING))) != 0) );
			setState(65);
			match(T__5);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IfelseContext extends ParserRuleContext {
		public TerminalNode IF() { return getToken(TJParser.IF, 0); }
		public RelationalExprContext relationalExpr() {
			return getRuleContext(RelationalExprContext.class,0);
		}
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public TerminalNode ELSE() { return getToken(TJParser.ELSE, 0); }
		public List<Statement2Context> statement2() {
			return getRuleContexts(Statement2Context.class);
		}
		public Statement2Context statement2(int i) {
			return getRuleContext(Statement2Context.class,i);
		}
		public IfelseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ifelse; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TJVisitor ) return ((TJVisitor<? extends T>)visitor).visitIfelse(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IfelseContext ifelse() throws RecognitionException {
		IfelseContext _localctx = new IfelseContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_ifelse);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(67);
			match(IF);
			setState(68);
			match(T__1);
			setState(69);
			relationalExpr();
			setState(70);
			match(T__3);
			setState(71);
			match(T__4);
			setState(73); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(72);
				statement();
				}
				}
				setState(75); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << T__6) | (1L << T__15) | (1L << VAR) | (1L << IF) | (1L << INT) | (1L << BOOL) | (1L << ID) | (1L << STRING))) != 0) );
			setState(77);
			match(T__5);
			setState(87);
			_la = _input.LA(1);
			if (_la==ELSE) {
				{
				setState(78);
				match(ELSE);
				setState(79);
				match(T__4);
				setState(81); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(80);
					statement2();
					}
					}
					setState(83); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << T__6) | (1L << T__15) | (1L << VAR) | (1L << IF) | (1L << INT) | (1L << BOOL) | (1L << ID) | (1L << STRING))) != 0) );
				setState(85);
				match(T__5);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WhileloopContext extends ParserRuleContext {
		public RelationalExprContext relationalExpr() {
			return getRuleContext(RelationalExprContext.class,0);
		}
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public WhileloopContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_whileloop; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TJVisitor ) return ((TJVisitor<? extends T>)visitor).visitWhileloop(this);
			else return visitor.visitChildren(this);
		}
	}

	public final WhileloopContext whileloop() throws RecognitionException {
		WhileloopContext _localctx = new WhileloopContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_whileloop);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(89);
			match(T__6);
			setState(90);
			match(T__1);
			setState(91);
			relationalExpr();
			setState(92);
			match(T__3);
			setState(93);
			match(T__4);
			setState(95); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(94);
				statement();
				}
				}
				setState(97); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << T__6) | (1L << T__15) | (1L << VAR) | (1L << IF) | (1L << INT) | (1L << BOOL) | (1L << ID) | (1L << STRING))) != 0) );
			setState(99);
			match(T__5);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AssignmentContext extends ParserRuleContext {
		public AssignmentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assignment; }
	 
		public AssignmentContext() { }
		public void copyFrom(AssignmentContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class AssigContext extends AssignmentContext {
		public TerminalNode ID() { return getToken(TJParser.ID, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode SEMICOLON() { return getToken(TJParser.SEMICOLON, 0); }
		public TerminalNode VAR() { return getToken(TJParser.VAR, 0); }
		public AssigContext(AssignmentContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TJVisitor ) return ((TJVisitor<? extends T>)visitor).visitAssig(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NoassignContext extends AssignmentContext {
		public TerminalNode ID() { return getToken(TJParser.ID, 0); }
		public TerminalNode SEMICOLON() { return getToken(TJParser.SEMICOLON, 0); }
		public TerminalNode VAR() { return getToken(TJParser.VAR, 0); }
		public NoassignContext(AssignmentContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TJVisitor ) return ((TJVisitor<? extends T>)visitor).visitNoassign(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AssignmentContext assignment() throws RecognitionException {
		AssignmentContext _localctx = new AssignmentContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_assignment);
		int _la;
		try {
			setState(114);
			switch ( getInterpreter().adaptivePredict(_input,11,_ctx) ) {
			case 1:
				_localctx = new AssigContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(102);
				_la = _input.LA(1);
				if (_la==VAR) {
					{
					setState(101);
					match(VAR);
					}
				}

				setState(104);
				match(ID);
				setState(105);
				match(ASSIGN);
				setState(106);
				expr(0);
				setState(107);
				match(SEMICOLON);
				}
				break;
			case 2:
				_localctx = new NoassignContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(110);
				_la = _input.LA(1);
				if (_la==VAR) {
					{
					setState(109);
					match(VAR);
					}
				}

				setState(112);
				match(ID);
				setState(113);
				match(SEMICOLON);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StackOpContext extends ParserRuleContext {
		public StackOpContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_stackOp; }
	 
		public StackOpContext() { }
		public void copyFrom(StackOpContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class AssignStackContext extends StackOpContext {
		public TerminalNode ID() { return getToken(TJParser.ID, 0); }
		public TerminalNode ASSIGN() { return getToken(TJParser.ASSIGN, 0); }
		public TerminalNode STACK() { return getToken(TJParser.STACK, 0); }
		public TerminalNode SEMICOLON() { return getToken(TJParser.SEMICOLON, 0); }
		public TerminalNode VAR() { return getToken(TJParser.VAR, 0); }
		public AssignStackContext(StackOpContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TJVisitor ) return ((TJVisitor<? extends T>)visitor).visitAssignStack(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class PushStackContext extends StackOpContext {
		public TerminalNode ID() { return getToken(TJParser.ID, 0); }
		public TerminalNode PUSH() { return getToken(TJParser.PUSH, 0); }
		public TermContext term() {
			return getRuleContext(TermContext.class,0);
		}
		public TerminalNode SEMICOLON() { return getToken(TJParser.SEMICOLON, 0); }
		public PushStackContext(StackOpContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TJVisitor ) return ((TJVisitor<? extends T>)visitor).visitPushStack(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class PopStackContext extends StackOpContext {
		public List<TerminalNode> ID() { return getTokens(TJParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(TJParser.ID, i);
		}
		public TerminalNode POP() { return getToken(TJParser.POP, 0); }
		public TerminalNode SEMICOLON() { return getToken(TJParser.SEMICOLON, 0); }
		public TerminalNode ASSIGN() { return getToken(TJParser.ASSIGN, 0); }
		public TerminalNode VAR() { return getToken(TJParser.VAR, 0); }
		public PopStackContext(StackOpContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TJVisitor ) return ((TJVisitor<? extends T>)visitor).visitPopStack(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StackOpContext stackOp() throws RecognitionException {
		StackOpContext _localctx = new StackOpContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_stackOp);
		int _la;
		try {
			setState(143);
			switch ( getInterpreter().adaptivePredict(_input,15,_ctx) ) {
			case 1:
				_localctx = new PushStackContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(116);
				match(ID);
				setState(117);
				match(T__7);
				setState(118);
				match(PUSH);
				setState(119);
				match(T__1);
				setState(120);
				term();
				setState(121);
				match(T__3);
				setState(122);
				match(SEMICOLON);
				}
				break;
			case 2:
				_localctx = new PopStackContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(129);
				switch ( getInterpreter().adaptivePredict(_input,13,_ctx) ) {
				case 1:
					{
					setState(125);
					_la = _input.LA(1);
					if (_la==VAR) {
						{
						setState(124);
						match(VAR);
						}
					}

					setState(127);
					match(ID);
					setState(128);
					match(ASSIGN);
					}
					break;
				}
				setState(131);
				match(ID);
				setState(132);
				match(T__7);
				setState(133);
				match(POP);
				setState(134);
				match(T__8);
				setState(135);
				match(SEMICOLON);
				}
				break;
			case 3:
				_localctx = new AssignStackContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(137);
				_la = _input.LA(1);
				if (_la==VAR) {
					{
					setState(136);
					match(VAR);
					}
				}

				setState(139);
				match(ID);
				setState(140);
				match(ASSIGN);
				setState(141);
				match(STACK);
				setState(142);
				match(SEMICOLON);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RelationalExprContext extends ParserRuleContext {
		public RelationalExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_relationalExpr; }
	 
		public RelationalExprContext() { }
		public void copyFrom(RelationalExprContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class LtContext extends RelationalExprContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public LtContext(RelationalExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TJVisitor ) return ((TJVisitor<? extends T>)visitor).visitLt(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class GteContext extends RelationalExprContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public GteContext(RelationalExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TJVisitor ) return ((TJVisitor<? extends T>)visitor).visitGte(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NeqContext extends RelationalExprContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public NeqContext(RelationalExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TJVisitor ) return ((TJVisitor<? extends T>)visitor).visitNeq(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class LteContext extends RelationalExprContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public LteContext(RelationalExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TJVisitor ) return ((TJVisitor<? extends T>)visitor).visitLte(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class EqContext extends RelationalExprContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public EqContext(RelationalExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TJVisitor ) return ((TJVisitor<? extends T>)visitor).visitEq(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class GtContext extends RelationalExprContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public GtContext(RelationalExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TJVisitor ) return ((TJVisitor<? extends T>)visitor).visitGt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RelationalExprContext relationalExpr() throws RecognitionException {
		RelationalExprContext _localctx = new RelationalExprContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_relationalExpr);
		try {
			setState(169);
			switch ( getInterpreter().adaptivePredict(_input,16,_ctx) ) {
			case 1:
				_localctx = new GtContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(145);
				expr(0);
				setState(146);
				match(T__9);
				setState(147);
				expr(0);
				}
				break;
			case 2:
				_localctx = new LtContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(149);
				expr(0);
				setState(150);
				match(T__10);
				setState(151);
				expr(0);
				}
				break;
			case 3:
				_localctx = new LteContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(153);
				expr(0);
				setState(154);
				match(T__11);
				setState(155);
				expr(0);
				}
				break;
			case 4:
				_localctx = new GteContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(157);
				expr(0);
				setState(158);
				match(T__12);
				setState(159);
				expr(0);
				}
				break;
			case 5:
				_localctx = new EqContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(161);
				expr(0);
				setState(162);
				match(T__13);
				setState(163);
				expr(0);
				}
				break;
			case 6:
				_localctx = new NeqContext(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				setState(165);
				expr(0);
				setState(166);
				match(T__14);
				setState(167);
				expr(0);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExprContext extends ParserRuleContext {
		public ExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expr; }
	 
		public ExprContext() { }
		public void copyFrom(ExprContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class PlusminusContext extends ExprContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode PLUSMIN() { return getToken(TJParser.PLUSMIN, 0); }
		public PlusminusContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TJVisitor ) return ((TJVisitor<? extends T>)visitor).visitPlusminus(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class FuncallContext extends ExprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TermContext term() {
			return getRuleContext(TermContext.class,0);
		}
		public TerminalNode SEMICOLON() { return getToken(TJParser.SEMICOLON, 0); }
		public FuncallContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TJVisitor ) return ((TJVisitor<? extends T>)visitor).visitFuncall(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class TermexprContext extends ExprContext {
		public TermContext term() {
			return getRuleContext(TermContext.class,0);
		}
		public TermexprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TJVisitor ) return ((TJVisitor<? extends T>)visitor).visitTermexpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class MuldivContext extends ExprContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode MULD() { return getToken(TJParser.MULD, 0); }
		public MuldivContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TJVisitor ) return ((TJVisitor<? extends T>)visitor).visitMuldiv(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExprContext expr() throws RecognitionException {
		return expr(0);
	}

	private ExprContext expr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ExprContext _localctx = new ExprContext(_ctx, _parentState);
		ExprContext _prevctx = _localctx;
		int _startState = 18;
		enterRecursionRule(_localctx, 18, RULE_expr, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			_localctx = new TermexprContext(_localctx);
			_ctx = _localctx;
			_prevctx = _localctx;

			setState(172);
			term();
			}
			_ctx.stop = _input.LT(-1);
			setState(188);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,18,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(186);
					switch ( getInterpreter().adaptivePredict(_input,17,_ctx) ) {
					case 1:
						{
						_localctx = new MuldivContext(new ExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(174);
						if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
						setState(175);
						match(MULD);
						setState(176);
						expr(5);
						}
						break;
					case 2:
						{
						_localctx = new PlusminusContext(new ExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(177);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(178);
						match(PLUSMIN);
						setState(179);
						expr(4);
						}
						break;
					case 3:
						{
						_localctx = new FuncallContext(new ExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(180);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(181);
						match(T__1);
						setState(182);
						term();
						setState(183);
						match(T__3);
						setState(184);
						match(SEMICOLON);
						}
						break;
					}
					} 
				}
				setState(190);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,18,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class PrintContext extends ParserRuleContext {
		public TerminalNode SEMICOLON() { return getToken(TJParser.SEMICOLON, 0); }
		public List<TermContext> term() {
			return getRuleContexts(TermContext.class);
		}
		public TermContext term(int i) {
			return getRuleContext(TermContext.class,i);
		}
		public PrintContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_print; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TJVisitor ) return ((TJVisitor<? extends T>)visitor).visitPrint(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PrintContext print() throws RecognitionException {
		PrintContext _localctx = new PrintContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_print);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(191);
			match(T__15);
			setState(193); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(192);
				term();
				}
				}
				setState(195); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << INT) | (1L << BOOL) | (1L << ID) | (1L << STRING))) != 0) );
			setState(197);
			match(SEMICOLON);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TermContext extends ParserRuleContext {
		public TermContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_term; }
	 
		public TermContext() { }
		public void copyFrom(TermContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class BoolContext extends TermContext {
		public TerminalNode BOOL() { return getToken(TJParser.BOOL, 0); }
		public BoolContext(TermContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TJVisitor ) return ((TJVisitor<? extends T>)visitor).visitBool(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class StringContext extends TermContext {
		public TerminalNode STRING() { return getToken(TJParser.STRING, 0); }
		public StringContext(TermContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TJVisitor ) return ((TJVisitor<? extends T>)visitor).visitString(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IdContext extends TermContext {
		public TerminalNode ID() { return getToken(TJParser.ID, 0); }
		public IdContext(TermContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TJVisitor ) return ((TJVisitor<? extends T>)visitor).visitId(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IntContext extends TermContext {
		public TerminalNode INT() { return getToken(TJParser.INT, 0); }
		public IntContext(TermContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TJVisitor ) return ((TJVisitor<? extends T>)visitor).visitInt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TermContext term() throws RecognitionException {
		TermContext _localctx = new TermContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_term);
		try {
			setState(203);
			switch (_input.LA(1)) {
			case ID:
				_localctx = new IdContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(199);
				match(ID);
				}
				break;
			case INT:
				_localctx = new IntContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(200);
				match(INT);
				}
				break;
			case BOOL:
				_localctx = new BoolContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(201);
				match(BOOL);
				}
				break;
			case STRING:
				_localctx = new StringContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(202);
				match(STRING);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 9:
			return expr_sempred((ExprContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean expr_sempred(ExprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 4);
		case 1:
			return precpred(_ctx, 3);
		case 2:
			return precpred(_ctx, 2);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3!\u00d0\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\3\2\6\2\34\n\2\r\2\16\2\35\3\3\3\3\3\3\3\3\3\3\3\3"+
		"\3\3\5\3\'\n\3\3\4\3\4\3\4\3\4\3\4\3\4\3\4\5\4\60\n\4\3\5\3\5\3\5\3\5"+
		"\3\5\3\5\7\58\n\5\f\5\16\5;\13\5\3\5\3\5\3\5\6\5@\n\5\r\5\16\5A\3\5\3"+
		"\5\3\6\3\6\3\6\3\6\3\6\3\6\6\6L\n\6\r\6\16\6M\3\6\3\6\3\6\3\6\6\6T\n\6"+
		"\r\6\16\6U\3\6\3\6\5\6Z\n\6\3\7\3\7\3\7\3\7\3\7\3\7\6\7b\n\7\r\7\16\7"+
		"c\3\7\3\7\3\b\5\bi\n\b\3\b\3\b\3\b\3\b\3\b\3\b\5\bq\n\b\3\b\3\b\5\bu\n"+
		"\b\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\5\t\u0080\n\t\3\t\3\t\5\t\u0084"+
		"\n\t\3\t\3\t\3\t\3\t\3\t\3\t\5\t\u008c\n\t\3\t\3\t\3\t\3\t\5\t\u0092\n"+
		"\t\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n"+
		"\3\n\3\n\3\n\3\n\3\n\3\n\3\n\5\n\u00ac\n\n\3\13\3\13\3\13\3\13\3\13\3"+
		"\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\7\13\u00bd\n\13\f\13"+
		"\16\13\u00c0\13\13\3\f\3\f\6\f\u00c4\n\f\r\f\16\f\u00c5\3\f\3\f\3\r\3"+
		"\r\3\r\3\r\5\r\u00ce\n\r\3\r\2\3\24\16\2\4\6\b\n\f\16\20\22\24\26\30\2"+
		"\2\u00ea\2\33\3\2\2\2\4&\3\2\2\2\6/\3\2\2\2\b\61\3\2\2\2\nE\3\2\2\2\f"+
		"[\3\2\2\2\16t\3\2\2\2\20\u0091\3\2\2\2\22\u00ab\3\2\2\2\24\u00ad\3\2\2"+
		"\2\26\u00c1\3\2\2\2\30\u00cd\3\2\2\2\32\34\5\4\3\2\33\32\3\2\2\2\34\35"+
		"\3\2\2\2\35\33\3\2\2\2\35\36\3\2\2\2\36\3\3\2\2\2\37\'\5\b\5\2 \'\5\n"+
		"\6\2!\'\5\f\7\2\"\'\5\16\b\2#\'\5\26\f\2$\'\5\24\13\2%\'\5\20\t\2&\37"+
		"\3\2\2\2& \3\2\2\2&!\3\2\2\2&\"\3\2\2\2&#\3\2\2\2&$\3\2\2\2&%\3\2\2\2"+
		"\'\5\3\2\2\2(\60\5\b\5\2)\60\5\n\6\2*\60\5\f\7\2+\60\5\16\b\2,\60\5\26"+
		"\f\2-\60\5\24\13\2.\60\5\20\t\2/(\3\2\2\2/)\3\2\2\2/*\3\2\2\2/+\3\2\2"+
		"\2/,\3\2\2\2/-\3\2\2\2/.\3\2\2\2\60\7\3\2\2\2\61\62\7\3\2\2\62\63\7\37"+
		"\2\2\63\64\7\4\2\2\649\5\30\r\2\65\66\7\5\2\2\668\5\30\r\2\67\65\3\2\2"+
		"\28;\3\2\2\29\67\3\2\2\29:\3\2\2\2:<\3\2\2\2;9\3\2\2\2<=\7\6\2\2=?\7\7"+
		"\2\2>@\5\4\3\2?>\3\2\2\2@A\3\2\2\2A?\3\2\2\2AB\3\2\2\2BC\3\2\2\2CD\7\b"+
		"\2\2D\t\3\2\2\2EF\7\33\2\2FG\7\4\2\2GH\5\22\n\2HI\7\6\2\2IK\7\7\2\2JL"+
		"\5\4\3\2KJ\3\2\2\2LM\3\2\2\2MK\3\2\2\2MN\3\2\2\2NO\3\2\2\2OY\7\b\2\2P"+
		"Q\7\34\2\2QS\7\7\2\2RT\5\6\4\2SR\3\2\2\2TU\3\2\2\2US\3\2\2\2UV\3\2\2\2"+
		"VW\3\2\2\2WX\7\b\2\2XZ\3\2\2\2YP\3\2\2\2YZ\3\2\2\2Z\13\3\2\2\2[\\\7\t"+
		"\2\2\\]\7\4\2\2]^\5\22\n\2^_\7\6\2\2_a\7\7\2\2`b\5\4\3\2a`\3\2\2\2bc\3"+
		"\2\2\2ca\3\2\2\2cd\3\2\2\2de\3\2\2\2ef\7\b\2\2f\r\3\2\2\2gi\7\27\2\2h"+
		"g\3\2\2\2hi\3\2\2\2ij\3\2\2\2jk\7\37\2\2kl\7\25\2\2lm\5\24\13\2mn\7\26"+
		"\2\2nu\3\2\2\2oq\7\27\2\2po\3\2\2\2pq\3\2\2\2qr\3\2\2\2rs\7\37\2\2su\7"+
		"\26\2\2th\3\2\2\2tp\3\2\2\2u\17\3\2\2\2vw\7\37\2\2wx\7\n\2\2xy\7\31\2"+
		"\2yz\7\4\2\2z{\5\30\r\2{|\7\6\2\2|}\7\26\2\2}\u0092\3\2\2\2~\u0080\7\27"+
		"\2\2\177~\3\2\2\2\177\u0080\3\2\2\2\u0080\u0081\3\2\2\2\u0081\u0082\7"+
		"\37\2\2\u0082\u0084\7\25\2\2\u0083\177\3\2\2\2\u0083\u0084\3\2\2\2\u0084"+
		"\u0085\3\2\2\2\u0085\u0086\7\37\2\2\u0086\u0087\7\n\2\2\u0087\u0088\7"+
		"\32\2\2\u0088\u0089\7\13\2\2\u0089\u0092\7\26\2\2\u008a\u008c\7\27\2\2"+
		"\u008b\u008a\3\2\2\2\u008b\u008c\3\2\2\2\u008c\u008d\3\2\2\2\u008d\u008e"+
		"\7\37\2\2\u008e\u008f\7\25\2\2\u008f\u0090\7\30\2\2\u0090\u0092\7\26\2"+
		"\2\u0091v\3\2\2\2\u0091\u0083\3\2\2\2\u0091\u008b\3\2\2\2\u0092\21\3\2"+
		"\2\2\u0093\u0094\5\24\13\2\u0094\u0095\7\f\2\2\u0095\u0096\5\24\13\2\u0096"+
		"\u00ac\3\2\2\2\u0097\u0098\5\24\13\2\u0098\u0099\7\r\2\2\u0099\u009a\5"+
		"\24\13\2\u009a\u00ac\3\2\2\2\u009b\u009c\5\24\13\2\u009c\u009d\7\16\2"+
		"\2\u009d\u009e\5\24\13\2\u009e\u00ac\3\2\2\2\u009f\u00a0\5\24\13\2\u00a0"+
		"\u00a1\7\17\2\2\u00a1\u00a2\5\24\13\2\u00a2\u00ac\3\2\2\2\u00a3\u00a4"+
		"\5\24\13\2\u00a4\u00a5\7\20\2\2\u00a5\u00a6\5\24\13\2\u00a6\u00ac\3\2"+
		"\2\2\u00a7\u00a8\5\24\13\2\u00a8\u00a9\7\21\2\2\u00a9\u00aa\5\24\13\2"+
		"\u00aa\u00ac\3\2\2\2\u00ab\u0093\3\2\2\2\u00ab\u0097\3\2\2\2\u00ab\u009b"+
		"\3\2\2\2\u00ab\u009f\3\2\2\2\u00ab\u00a3\3\2\2\2\u00ab\u00a7\3\2\2\2\u00ac"+
		"\23\3\2\2\2\u00ad\u00ae\b\13\1\2\u00ae\u00af\5\30\r\2\u00af\u00be\3\2"+
		"\2\2\u00b0\u00b1\f\6\2\2\u00b1\u00b2\7\23\2\2\u00b2\u00bd\5\24\13\7\u00b3"+
		"\u00b4\f\5\2\2\u00b4\u00b5\7\24\2\2\u00b5\u00bd\5\24\13\6\u00b6\u00b7"+
		"\f\4\2\2\u00b7\u00b8\7\4\2\2\u00b8\u00b9\5\30\r\2\u00b9\u00ba\7\6\2\2"+
		"\u00ba\u00bb\7\26\2\2\u00bb\u00bd\3\2\2\2\u00bc\u00b0\3\2\2\2\u00bc\u00b3"+
		"\3\2\2\2\u00bc\u00b6\3\2\2\2\u00bd\u00c0\3\2\2\2\u00be\u00bc\3\2\2\2\u00be"+
		"\u00bf\3\2\2\2\u00bf\25\3\2\2\2\u00c0\u00be\3\2\2\2\u00c1\u00c3\7\22\2"+
		"\2\u00c2\u00c4\5\30\r\2\u00c3\u00c2\3\2\2\2\u00c4\u00c5\3\2\2\2\u00c5"+
		"\u00c3\3\2\2\2\u00c5\u00c6\3\2\2\2\u00c6\u00c7\3\2\2\2\u00c7\u00c8\7\26"+
		"\2\2\u00c8\27\3\2\2\2\u00c9\u00ce\7\37\2\2\u00ca\u00ce\7\35\2\2\u00cb"+
		"\u00ce\7\36\2\2\u00cc\u00ce\7 \2\2\u00cd\u00c9\3\2\2\2\u00cd\u00ca\3\2"+
		"\2\2\u00cd\u00cb\3\2\2\2\u00cd\u00cc\3\2\2\2\u00ce\31\3\2\2\2\27\35&/"+
		"9AMUYchpt\177\u0083\u008b\u0091\u00ab\u00bc\u00be\u00c5\u00cd";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}