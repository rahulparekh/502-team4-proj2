// Generated from TJ.g4 by ANTLR 4.5
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link TJParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface TJVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link TJParser#start}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStart(TJParser.StartContext ctx);
	/**
	 * Visit a parse tree produced by {@link TJParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatement(TJParser.StatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link TJParser#statement2}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatement2(TJParser.Statement2Context ctx);
	/**
	 * Visit a parse tree produced by {@link TJParser#functionstmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionstmt(TJParser.FunctionstmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link TJParser#ifelse}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIfelse(TJParser.IfelseContext ctx);
	/**
	 * Visit a parse tree produced by {@link TJParser#whileloop}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhileloop(TJParser.WhileloopContext ctx);
	/**
	 * Visit a parse tree produced by the {@code assig}
	 * labeled alternative in {@link TJParser#assignment}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssig(TJParser.AssigContext ctx);
	/**
	 * Visit a parse tree produced by the {@code noassign}
	 * labeled alternative in {@link TJParser#assignment}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNoassign(TJParser.NoassignContext ctx);
	/**
	 * Visit a parse tree produced by the {@code pushStack}
	 * labeled alternative in {@link TJParser#stackOp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPushStack(TJParser.PushStackContext ctx);
	/**
	 * Visit a parse tree produced by the {@code popStack}
	 * labeled alternative in {@link TJParser#stackOp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPopStack(TJParser.PopStackContext ctx);
	/**
	 * Visit a parse tree produced by the {@code assignStack}
	 * labeled alternative in {@link TJParser#stackOp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssignStack(TJParser.AssignStackContext ctx);
	/**
	 * Visit a parse tree produced by the {@code gt}
	 * labeled alternative in {@link TJParser#relationalExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGt(TJParser.GtContext ctx);
	/**
	 * Visit a parse tree produced by the {@code lt}
	 * labeled alternative in {@link TJParser#relationalExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLt(TJParser.LtContext ctx);
	/**
	 * Visit a parse tree produced by the {@code lte}
	 * labeled alternative in {@link TJParser#relationalExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLte(TJParser.LteContext ctx);
	/**
	 * Visit a parse tree produced by the {@code gte}
	 * labeled alternative in {@link TJParser#relationalExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGte(TJParser.GteContext ctx);
	/**
	 * Visit a parse tree produced by the {@code eq}
	 * labeled alternative in {@link TJParser#relationalExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEq(TJParser.EqContext ctx);
	/**
	 * Visit a parse tree produced by the {@code neq}
	 * labeled alternative in {@link TJParser#relationalExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNeq(TJParser.NeqContext ctx);
	/**
	 * Visit a parse tree produced by the {@code plusminus}
	 * labeled alternative in {@link TJParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPlusminus(TJParser.PlusminusContext ctx);
	/**
	 * Visit a parse tree produced by the {@code funcall}
	 * labeled alternative in {@link TJParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFuncall(TJParser.FuncallContext ctx);
	/**
	 * Visit a parse tree produced by the {@code termexpr}
	 * labeled alternative in {@link TJParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTermexpr(TJParser.TermexprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code muldiv}
	 * labeled alternative in {@link TJParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMuldiv(TJParser.MuldivContext ctx);
	/**
	 * Visit a parse tree produced by {@link TJParser#print}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrint(TJParser.PrintContext ctx);
	/**
	 * Visit a parse tree produced by the {@code id}
	 * labeled alternative in {@link TJParser#term}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitId(TJParser.IdContext ctx);
	/**
	 * Visit a parse tree produced by the {@code int}
	 * labeled alternative in {@link TJParser#term}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInt(TJParser.IntContext ctx);
	/**
	 * Visit a parse tree produced by the {@code bool}
	 * labeled alternative in {@link TJParser#term}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBool(TJParser.BoolContext ctx);
	/**
	 * Visit a parse tree produced by the {@code string}
	 * labeled alternative in {@link TJParser#term}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitString(TJParser.StringContext ctx);
}