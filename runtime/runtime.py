import sys
import re
import operator

sys.setrecursionlimit(50);

# operator module
# makes it possible to pass operator
# a variable
ops = {"+": operator.add,
       "-": operator.sub,
       "*": operator.mul,
       "/": operator.div,
       ">": operator.gt,
       "<": operator.lt,
       ">=": operator.ge,
       "<=": operator.le,
       "==": operator.eq,
       "!=": operator.ne,
       }

# lexer tokens
OPERATORS = 'OPERATORS'
INT = 'INT'
ID = 'ID'
STRING = 'STRING'
PUSH = 'PUSH'
PRINT = 'PRINT'
ASSIGN = 'ASSIGN'
CONDITION = 'CONDITION'
IFTRUE = 'IFTRUE'
IFFALSE = 'IFFALSE'
GOTO = 'GOTO'
BOOLEAN = 'BOOLEAN'
ENDC = 'ENDC'
BEGIN_FUNCT = "BEGIN_FUNCT"
FUNCTION = 'FUNCTION'
ENDFUNCTION = 'ENDFUNCTION'
CALL_FUNCT = 'CALL_FUNCT'
RETURN = 'RETURN'
STACK = 'RESSTACK'
ASSIGNSTACK = 'ASSIGNSTACK'
POP = 'POP'

token_exprs = [
    (r'[ \n\t]+',               None),
    (r'//.*',                   None),
    (r'\+',                     OPERATORS),
    (r'-',                      OPERATORS),
    (r'\*',                     OPERATORS),
    (r'/',                      OPERATORS),
    (r'<=',                     OPERATORS),
    (r'<',                      OPERATORS),
    (r'>=',                     OPERATORS),
    (r'>',                      OPERATORS),
    (r'!=',                     OPERATORS),
    (r'==',                     OPERATORS),
    (r'TRUE',                   BOOLEAN),
    (r'FALSE',                  BOOLEAN),
    (r'POP',                    POP),
    (r'PUSH',                   PUSH),
    (r'ASSIGNSTACK',            ASSIGNSTACK),
    (r'ASSIGN',                 ASSIGN),
    (r'PRINT',                  PRINT),
    (r'CONDITION.+',            CONDITION),
    (r'GOTO.+',                 GOTO),
    (r'IFTRUE',                 IFTRUE),
    (r'IFFALSE',                IFFALSE),
    (r'ENDC.+',                 ENDC),
    (r'BEGIN_FUNCT',            BEGIN_FUNCT),
    (r'FUNCTION.+',             FUNCTION),
    (r'ENDFUNCTION',            ENDFUNCTION),
    (r'CALL_FUNCT.+',           CALL_FUNCT),
    (r'RESSTACK',               STACK),
    # (r'RETURN',                 RETURN),
    (r'[0-9]+',                 INT),
    (r'[A-Za-z_][A-Za-z0-9_]*', ID),
    (r'\"(\\.|[^\"])*\"',       STRING),
]


def lex(characters, token_exprs):
    pos = 0
    tokens = []
    while pos < len(characters):
        match = None
        for token_expr in token_exprs:
            pattern, tag = token_expr
            regex = re.compile(pattern)
            match = regex.match(characters, pos)
            if match:
                text = match.group(0)
                if tag:
                    token = (text, tag)
                    tokens.append(token)
                break
        if not match:
            sys.stderr.write('Illegal character: %s\\n' % characters[pos])
            sys.exit(1)
        else:
            pos = match.end(0)
    return tokens


def interpreter(tokens, stack, symbolTable, conditions, functionsTable):

    index = 0

    # end the loop if index
    # has exceeded tokens length
    while index < len(tokens):

        # print "my token: ",tokens[index]
        if tokens[index][1] == PUSH:
            # print "inside push"

            index += 1

            # add all to stack
            if tokens[index][1] == INT:
                symbol = int(tokens[index][0])
            elif tokens[index][1] == STACK:
                symbol = []
            else:
                symbol = tokens[index][0]

            stack.append(symbol)

            # if ID add to symbol
            # table as well
            if tokens[index][1] == ID:
                if symbol not in symbolTable:
                    symbolTable[symbol] = None

        if tokens[index][1] == ASSIGN:
            symbol = stack.pop()
            value = stack.pop()
            stack.append(symbol)
            symbolTable[symbol] = value

        if tokens[index][1] == ASSIGNSTACK:
            symbol = stack.pop()
            value = stack.pop()
            stack.append(symbol)
            symbolTable[symbol].append(value)

        if tokens[index][1] == POP:
            index += 1;
            symbol = tokens[index][0];
            symbol = symbolTable[symbol].pop();
            stack.append(symbol);

        if tokens[index][1] == OPERATORS:
            item2 = stack.pop()
            item1 = stack.pop()

            if lex(str(item2), token_exprs)[0][1] == ID:
                item2 = symbolTable[item2]

            if lex(str(item1), token_exprs)[0][1] == ID:
                item1 = symbolTable[item1]

            op_func = ops[tokens[index][0]]
            stack.append(op_func(item2, item1))

        if tokens[index][1] == IFTRUE:
            item = stack.pop()
            ifCaseTokens = []

            index += 1
            label = tokens[index][0]
            index += 1

            # update the condition with variable name
            conditions[label] = item

            if item == False:
                stack.append(False)
                # skip to the end of the loop
                while tokens[index][0] != "END" + label:
                    index += 1
            else:
                # get the function instructions
                while tokens[index][0] != "END" + label:
                    ifCaseTokens.append(tokens[index])
                    index += 1

                # execute them recursively
                interpreter(ifCaseTokens, stack, symbolTable, conditions, functionsTable)

                # add the condition to the stack again
                stack.append(item)

        if tokens[index][1] == IFFALSE:
            item = stack.pop()
            ifCaseTokens = []

            index += 1
            label = tokens[index][0]
            index += 1

            if item == True:
                while tokens[index][0] != "END" + label:
                    index += 1
            else:
                while tokens[index][0] != "END" + label:
                    ifCaseTokens.append(tokens[index])
                    index += 1
                interpreter(ifCaseTokens, stack, symbolTable, conditions, functionsTable)
                stack.append(item)

        if tokens[index][1] == GOTO:

            labels = tokens[index][0].split(",")
            ifTrue = labels[1]
            label = labels[2]
            if conditions[ifTrue]:
                while tokens[index][0] != "CONDITION " + label:
                    index -= 1

        if tokens[index][1] == BEGIN_FUNCT:
            while tokens[index][1] != ENDFUNCTION:
                index += 1

        if tokens[index][1] == CALL_FUNCT:

            # parse function name and parameters
            functions = tokens[index][0][10:].strip()
            functions = functions.split(":")
            callFuncName = functions[0]
            callFuncParameters = functions[1].split(",")

            # add function name with true flag to conditions
            conditions[callFuncName] = True

            tempIndex = index
            index = 0
            funcTest = "FUNCTION " + callFuncName


            # check if function statement list already exists in the dict
            if callFuncName not in functionsTable:

                # find the function from the beginning
                while tokens[index][0] != FUNCTION:

                    # if it's found break out
                    if funcTest in tokens[index][0]:
                        break
                    index += 1

                functionListTokens = []

                # get the function statement list and store it
                while tokens[index][0] != ENDFUNCTION:
                    functionListTokens.append(tokens[index])
                    index += 1
                functionListTokens.append(tokens[index])

                functionsTable[callFuncName] = functionListTokens

            else:

                functionListTokens = functionsTable[callFuncName]

            # parse function parameters
            functions = functionListTokens[0][0][8:].strip()
            functions = functions.split(":")
            funcName = functions[0]
            funcParameters = functions[1].split(",")

            scopedSymbolTable = symbolTable.copy()

            # match paramters in symbol table
            if len(callFuncParameters) == len(funcParameters):
                for paramIndex in xrange(0, len(callFuncParameters)):
                    value = symbolTable[callFuncParameters[paramIndex]]
                    scopedSymbolTable[funcParameters[paramIndex]] = value
            else:
                print 'Parameters mismatch for the function ' + funcName
                sys.exit()

            interpreter(
                functionListTokens, stack, scopedSymbolTable, conditions, functionsTable)
            index = tempIndex
            scopedSymbolTable = {}

        if tokens[index][1] == RETURN:
            symbol = stack.pop()

            # if variable
            # find the value and print it
            if lex(str(symbol), token_exprs)[0][1] == ID:
                stack.append(symbolTable[symbol])
            else:
                stack.append(symbol)

            while tokens[index][1] != ENDFUNCTION:
                index += 1

            # break out of the recursive function so that ENDFUNCTION isn't executed
            break 

        if tokens[index][1] == ENDFUNCTION:
            stack.append(None)

        if tokens[index][1] == PRINT:
            symbol = stack.pop()

            # if variable
            # find the value and print it
            if lex(str(symbol), token_exprs)[0][1] == ID:
                print symbolTable[symbol]
            else:
                print symbol

        index += 1


if __name__ == "__main__":

    if len(sys.argv) <= 1:
        print "Error! No file given"
        sys.exit()

    # read the file
    with open(sys.argv[1] + ".tj.int", "r") as myfile:
        characters = myfile.read()

    # global symbol table
    symbolTable = {}
    # global stack
    stack = []
    # conditions stack
    conditions = {}

    # get the tokens from the lexer
    tokens = lex(characters, token_exprs)

    # begin the interpreter
    interpreter(tokens, stack, symbolTable, conditions, {})
